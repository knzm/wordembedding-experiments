# -*- coding: utf-8 -*-

from __future__ import print_function

import logging
import os
import random

import numpy
from gensim.models.word2vec import Word2Vec
from gensim import matutils

logger = None


def parse(f):
    category = None
    queries = []
    for line in f:
        if line.startswith(":"):
            if category is not None:
                yield category, queries
            category = line.decode('utf-8').rstrip()[2:]
            queries = []
            continue
        words = line.decode('utf-8').rstrip().split()
        queries.append(words)
    yield category, queries


def analogy(model, w1, w2, w3, w4, vocab_size=None):
    results = model.most_similar(
        positive=[w2, w3],
        negative=[w1],
        restrict_vocab=vocab_size,
        topn=10)
    for i in xrange(len(results)):
        if results[i][0] == w4:
            return i + 1
    return -1


def main(args):
    model_path = os.path.join(args.result_dir, "vectors.bin")
    fvocab = os.path.join(args.result_dir, "vocab.txt")
    if not os.path.exists(fvocab):
        fvocab = None
    model = Word2Vec.load_word2vec_format(
        model_path, fvocab=fvocab, binary=True)

    # sections = model.accuracy(args.infile_query, restrict_vocab=args.vocab_size)
    # return

    if args.vocab_size is not None:
        def is_valid_word(word):
            if word not in model.vocab:
                return False
            return model.vocab[word].index < args.vocab_size
    else:
        def is_valid_word(word):
            return word in model.vocab
    n_sem = n_sem_good = 0
    n_syn = n_syn_good = 0
    n_total_skip = 0
    total_errors = []
    for i, (category, queries) in enumerate(parse(args.infile_query)):
        is_syn_query = category.startswith("gram")
        n_query = 0
        n_good = 0
        n_skip = 0
        errors = []
        ranks = []
        for query in queries:
            words = [w.lower() for w in query]
            missing_words = [word for word in words if not is_valid_word(word)]
            if missing_words:
                # print(u"*** skip query: {0} {1} {2} {3}".format(
                #         *words).encode('utf-8'))
                n_skip += 1
                n_total_skip += 1
                continue
            n_query += 1

            vec1 = model[words[0]] - model[words[1]]
            vec2 = model[words[2]] - model[words[3]]
            errors.append(numpy.linalg.norm(vec1 - vec2))

            rank = analogy(model, *words, vocab_size=args.vocab_size)
            # print(category, rank, *words)
            if rank != -1:
                ranks.append(rank)
            if rank == 1:
                n_good += 1
                if is_syn_query:
                    n_syn_good += 1
                else:
                    n_sem_good += 1
            if is_syn_query:
                n_syn += 1
            else:
                n_sem += 1
        if len(ranks) > 0:
            avg_rank = float(sum(ranks)) / len(ranks)
        else:
            avg_rank = float("nan")
        print((u"{0}: {1:.2f}% ({2}/{3}; {4} skipped) "
               "avg error: {5:.3f} avg rank: {6:.1f} of {7}").format(
                category,
                float(n_good) / n_query * 100,
                n_good, n_query,
                n_skip,
                numpy.mean(errors),
                avg_rank, len(ranks)).encode('utf-8'))
        total_errors += errors

    print("---")
    print(("Total accuracy: {0:.2f}%  "
           "Semantic accuracy: {1:.2f}%  "
           "Syntactic accuracy: {2:.2f}%").format(
            float(n_sem_good + n_syn_good) / (n_sem + n_syn) * 100,
            float(n_sem_good) / n_sem * 100,
            float(n_syn_good) / n_syn * 100))
    print("Questions seen / total: {0} {1}  {2:.2f}%".format(
            n_sem + n_syn,
            n_sem + n_syn + n_total_skip,
            float(n_sem + n_syn) / (n_sem + n_syn + n_total_skip) * 100))
    print("Total errors: {0:.3f}".format(numpy.mean(total_errors)))


def configure_logging():
    global logger
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(levelname)s [%(name)s] %(message)s",
        datefmt="%H:%M:%S")
    logger = logging.getLogger()


def get_parser():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("result_dir")
    parser.add_argument("--vocab-size", type=int)
    parser.add_argument("--query-file", dest="infile_query",
                        type=argparse.FileType("r"))
    return parser


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    configure_logging()

    main(args)
