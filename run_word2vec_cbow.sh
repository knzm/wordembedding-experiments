#!/bin/sh -e

bindir=./bin
train_file=text8/text8
result_root=${result_root:-result}
result_dir=$result_root/text8_word2vec_cbow

cbow=1
size=200
window=8
negative=25
hs=0
sample=1e-4
threads=20
binary=0
iter=${epoch:-15}

output=${result_dir}/vectors.txt
save_vocab=${result_dir}/vocab.txt

mkdir -p $result_dir
$bindir/word2vec_cbow -train $train_file -output $output -cbow $cbow -size $size -window $window -negative $negative -hs $hs -sample $sample -threads $threads -binary $binary -iter $iter -save-vocab $save_vocab

rm -f $result_dir/vectors.txt.gz
gzip $result_dir/vectors.txt
