#!/bin/sh -e

train_file=text8/text8
vocab_file=text8/vocab.txt

min_count=5

virtualenvs/gensim/bin/python word_count.py $train_file --min-count $min_count > $vocab_file
