# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import os
import gzip
import tempfile
import shutil

from gensim.models.word2vec import Word2Vec


def main(result_dir):
    print("Unpacking text file...", file=sys.stderr)
    wf = tempfile.NamedTemporaryFile()
    gzipped_text_path = os.path.join(result_dir, "vectors.txt.gz")
    with gzip.open(gzipped_text_path) as f:
        shutil.copyfileobj(f, wf)
    wf.flush()
    print("Loading text file...", file=sys.stderr)
    model_path = wf.name
    fvocab = os.path.join(result_dir, "vocab.txt")
    if not os.path.exists(fvocab):
        fvocab = None
    model = Word2Vec.load_word2vec_format(
        model_path, fvocab=fvocab, binary=False)
    print("Saving binary file...", file=sys.stderr)
    binary_model_path = os.path.join(result_dir, "vectors.bin")
    model.save_word2vec_format(binary_model_path, binary=True)


if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
