# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import os

from gensim.models import word2vec

train_file = "text8/text8"
result_dir = os.environ.get("result_dir", "result/text8_gensim")

import logging
logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s',
    level=logging.INFO)


def main():
    epoch = int(os.environ.get("epoch", 15))
    save_intermediate_models = False

    data = word2vec.Text8Corpus(train_file)
    model = word2vec.Word2Vec(
        size=200,
        window=8, min_count=5,
        alpha=0.025, min_alpha=0.025,
        # iter=epoch,
        sg=0, sample=1e-4, hs=0, negative=25,
        workers=20)
    # print("*** Building vocabulary", file=sys.stderr)
    model.build_vocab(data)
    # print("*** Training model", file=sys.stderr)
    # model.train(data)
    for epoch in range(epoch):
        print("*** Training epoch={0} (alpha={1})".format(epoch, model.alpha),
              file=sys.stderr)
        model.train(data)
        model.alpha -= 0.0015
        model.min_alpha = model.alpha
        if save_intermediate_models:
            model.save_word2vec_format(
                os.path.join(result_dir, "vectors-{0:02d}.txt".format(epoch)),
                binary=False)

    print("*** Saving model", file=sys.stderr)
    model.save_word2vec_format(
        os.path.join(result_dir, "vectors.txt"), binary=False,
        fvocab=os.path.join(result_dir, "vocab.txt"))


if __name__ == '__main__':
    main()
