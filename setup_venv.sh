#!/bin/sh -e

mkdir -p virtualenvs

# gensim
if [ ! -d virtualenvs/gensim ]; then
  ./setup_venv_gensim.sh
fi

# chainer
if [ ! -d virtualenvs/chainer ]; then
  ./setup_venv_chainer.sh
fi

# chainer master
if [ ! -d virtualenvs/chainer-master ]; then
  ./setup_venv_chainer_dev.sh master
fi

# tensorflow with CPU
if [ ! -d virtualenvs/tensorflow-cpu ]; then
  ./setup_venv_tensorflow_cpu.sh
fi

# tensorflow with GPU
if [ ! -d virtualenvs/tensorflow-gpu ]; then
  ./setup_venv_tensorflow_gpu.sh
fi
