#!/bin/sh -e

mkdir -p text8

if [ ! -f text8/text8 ]; then
  cd text8
  wget http://mattmahoney.net/dc/text8.zip
  unzip -x text8.zip
  cd ..
fi

if [ ! -f text8/vocab.txt ] && [ -x bin/word_count ]; then
  ./run_wordcount.sh
fi
