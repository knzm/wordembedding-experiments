#!/bin/sh -e

if [ -z "${HAS_CUDA+x}" ]; then
  if [ ! -z "${CUDA_HOME+x}" ] || [ ! -z "${CUDA_PATH+x}" ] || [ ! -z "${CUDA_ROOT+x}" ] || type nvcc > /dev/null 2>&1; then
    HAS_CUDA=1
  else
    HAS_CUDA=0
  fi
fi

export HAS_CUDA

(cd build; ./build_all.sh)
./install.sh
./setup_venv.sh
./setup_corpus.sh
