#!/bin/sh -e

virtualenv=${virtualenv:-virtualenv}

branch=${1:-master}

$virtualenv virtualenvs/chainer-${branch}
virtualenvs/chainer-${branch}/bin/pip install -U pip

for pkg in cython numpy h5py; do
  virtualenvs/chainer-${branch}/bin/pip install $pkg
done

(
  . virtualenvs/chainer-${branch}/bin/activate;
  cd build;
  ./install_chainer.sh ${branch}
)
