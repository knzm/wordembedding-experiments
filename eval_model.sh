#!/bin/sh -e

result_dir=$1
threshold=30000

# ./bin/compute-accuracy $result_dir/vectors.bin $threshold < data/questions-words.txt > $result_dir/accuracy.txt
# grep "ACCURACY TOP1" $result_dir/accuracy.txt \
#  | perl -ne 'if (/\((\d+) \/ (\d+)\)/){$a+=$1/$2; $b++}END{printf"%.2f%%\n",$a/$b*100}'

if [ ! -f $result_dir/vectors.bin ]; then
  python convert_model.py $result_dir
fi

virtualenvs/gensim/bin/python -u eval_model.py $result_dir --query-file data/questions-words.txt --vocab-size $threshold | tee $result_dir/accuracy.txt
