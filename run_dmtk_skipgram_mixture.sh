#!/bin/sh -e

bindir=./bin
result_root=${result_root:-result}
result_dir=$result_root/text8_dmtk_skipgram_mixture

# Parameter w.r.t. SG-Mixture Training
size=50
train_file=text8/text8

# Please visit http://ms-dmtk.azurewebsites.net/word2vec_multi.html#p5 for wiki2014 and clueweb.
# In running multi machine version, please separate the file and distribute the subfiles into different machines.

vocab_file=text8/vocab.txt
binary=2
init_learning_rate=0.025
epoch=1
window=5
threads=8
min_count=5
EM_iteration=1
momentum=0.05

default_sense=1
# Default number of senses for multi sense words
sense_num_multi=5

# Three ways of specifying multi sense words, each with sense_num_multi prototypes:
# 1)Set top_n frequent words
# 2)Set top_ratio (lie between 0 to 1) frequent words
# 3)Write all these words into sense_file

top_n=0
top_ratio=0
sense_file=data/sense_file.txt

# Output files
binary_embedding_file=${result_dir}/emb.bin
text_embedding_file=${result_dir}/emb.txt
huff_tree_file=${result_dir}/huff.txt
outputlayer_binary_file=${result_dir}/emb_out.bin
outputlayer_text_file=${result_dir}/emb_out.txt

max_preload_size=5

# Number of sentences for each datablock.
# Warning: for wiki2014, set it to 50000, for clueweb09, set it to 750000. Other values are not tested.
data_block_size=50000

# Warning: enable pipeline in multiverso will lead to some performance drop
is_pipline=0

# Whether to store the multinomial parameters in its original form. If false, will store their log values instead.
store_multinomial=0

mkdir -p $result_dir
$bindir/distributed_skipgram_mixture -train_file $train_file -binary_embedding_file $binary_embedding_file -text_embedding_file $text_embedding_file -threads $threads -size $size -binary $binary -epoch $epoch -init_learning_rate $init_learning_rate -min_count $min_count -window $window -momentum $momentum -EM_iteration $EM_iteration -top_n $top_n -top_ratio $top_ratio -default_sense $default_sense -sense_num_multi $sense_num_multi -huff_tree_file $huff_tree_file -vocab_file $vocab_file -outputlayer_binary_file $outputlayer_binary_file -outputlayer_text_file $outputlayer_text_file -read_sense $read_sense -data_block_size $data_block_size -is_pipline $is_pipline -store_multinomial $store_multinomial -max_preload_size $max_preload_size
