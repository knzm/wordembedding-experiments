#!/bin/sh -e

rm -rf word2vec_cbow
git clone https://github.com/ChenglongChen/word2vec_cbow.git

cd word2vec_cbow
rm word2vec
make
cd ..
