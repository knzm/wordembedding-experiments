#!/bin/sh -e

if [ ! -d bazel ]; then
  git clone https://github.com/bazelbuild/bazel.git
  cd bazel
  git checkout tags/0.1.0
  ./compile.sh
  cd ..
fi

rm -rf tensorflow
git clone https://github.com/tensorflow/tensorflow
cd tensorflow
git checkout tags/0.6.0
git submodule update --init
PYTHON_BIN_PATH=$(which python) \
TF_NEED_CUDA=1 \
CUDA_TOOLKIT_PATH=/usr/local/cuda \
CUDNN_INSTALL_PATH=/usr/local/cuda \
TF_UNOFFICIAL_SETTING=1 \
TF_CUDA_COMPUTE_CAPABILITIES=3.0 \
./configure
#../bazel/output/bazel build -c opt --config=cuda //tensorflow/cc:tutorials_example_trainer
../bazel/output/bazel build -c opt --config=cuda //tensorflow/tools/pip_package:build_pip_package
bazel-bin/tensorflow/tools/pip_package/build_pip_package $PWD/dist
pip install dist/tensorflow-0.6.0-cp27-none-linux_x86_64.whl
cd ..
