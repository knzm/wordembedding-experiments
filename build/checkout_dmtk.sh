#!/bin/sh -e

projects="lightlda distributed_word_embedding distributed_skipgram_mixture"

mkdir -p dmtk && cd dmtk

rm -rf multiverso
git clone https://github.com/Microsoft/multiverso.git

for project in $projects; do
  rm -rf ${project}
  git clone https://github.com/Microsoft/${project}.git
  ln -s ../multiverso ${project}/
done

(cat <<EOF
ae5ffa66b309af1a54f91d0d5feb60d111b94466 multiverso
e6640ce2f72e2427c4f6892c7ebeabcac6d3cb0b lightlda
28cfe7a3ec8eaef021bbb6ac6e55911137d37380 distributed_word_embedding
4a79d12f2886808273cafb6c3e1aef6f8295a7e9 distributed_skipgram_mixture
EOF
) | while read line; do
  rev=$(echo $line | cut -f1 -d ' ')
  dir=$(echo $line | cut -f2 -d ' ')
  (cd $dir; git checkout $rev)
done
