#!/bin/sh -e

branch=${1:-master}

rm -rf gensim-${branch}
git clone https://github.com/piskvorky/gensim.git gensim-${branch}

cd gensim-${branch}
git checkout ${branch}
python setup.py develop
cd ..
