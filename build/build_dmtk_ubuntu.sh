#!/bin/sh -e

projects="lightlda distributed_word_embedding distributed_skipgram_mixture"

buildroot=$PWD
./checkout_dmtk.sh
cd dmtk

cd multiverso/third_party
CXX=g++-4.8 sh -e install.sh

cd ../..

patch -p0 < $buildroot/build_dmtk_ubuntu/Makefile.patch
make -j4 -C multiverso CXX=g++-4.8

for project in $projects; do
  make -j4 -C $project CXX=g++-4.8
done

# Bonus. build word_count command
make -C distributed_word_embedding/preprocess -f $buildroot/Makefile.wordcount  all

cd ..
