#!/bin/sh -e

if [ "$(uname)" = "Darwin" ]; then
  ./build_dmtk_mac.sh
else
  ./build_dmtk_ubuntu.sh
fi

./build_word2vec.sh

if [ "${HAS_CUDA:-0}" -ne 0 ]; then
  ./build_word2vec_cbow.sh
fi
