#!/bin/sh -e

rm -rf word2vec
svn checkout http://word2vec.googlecode.com/svn/trunk word2vec

cd word2vec
patch -p0 < ../word2vec.patch
make
cd ..
