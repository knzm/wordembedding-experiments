#!/bin/sh -e

if ! type g++-4.8 > /dev/null 2>&1; then
  echo "gcc 4.8 must be installed."
  echo "You can do it with homebrew:"
  echo
  echo "$ brew install homebrew/versions/gcc48"
  echo
  exit -1
fi

projects="lightlda distributed_word_embedding distributed_skipgram_mixture"

buildroot=$PWD
./checkout_dmtk.sh
cd dmtk

export HWLOC_LIBXML2_CFLAGS=`xml2-config --cflags`
export HWLOC_LIBXML2_LIBS=`xml2-config --libs`

cd multiverso/third_party
CXX=g++-4.8 sh -e install.sh

# Forcing the Mac OS X linker to choose a static library
# See https://wincent.com/wiki/Forcing_the_Mac_OS_X_linker_to_choose_a_static_library
rm -f lib/*.dylib

cd ..
make -j4 all CXX=g++-4.8

cd ..

patch -p0 < $buildroot/build_dmtk_mac/Makefile.patch
(cd distributed_word_embedding; patch -p1) < $buildroot/build_dmtk_mac/distributed_word_embedding.patch

for project in $projects; do
  make -j4 -C $project CXX=g++-4.8
done

# Bonus. build word_count command
make -C distributed_word_embedding/preprocess -f $buildroot/Makefile.wordcount  all

cd ..
