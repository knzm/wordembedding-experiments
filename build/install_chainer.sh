#!/bin/sh -e

branch=${1:-master}

rm -rf chainer-${branch}
git clone https://github.com/pfnet/chainer.git chainer-${branch}

cd chainer-${branch}
git checkout ${branch}
if [ "${HAS_CUDA:-0}" -ne 0 ]; then
  python setup.py develop
else
  python setup.py develop --cupy-no-cuda
fi
cd ..
