#!/bin/sh -e

PYTHON=virtualenvs/chainer/bin/python
# PYTHON=virtualenvs/chainer-master/bin/python
result_root=${result_root:-result}
result_dir=$result_root/text8_chainer

gpu=-1
unit=200
window=8
batchsize=1000
epoch=${epoch:-15}
model=cbow
out_type=ns

mkdir -p $result_dir

export result_dir
$PYTHON word2vec_chainer.py --gpu $gpu --unit $unit --window $window --batchsize $batchsize --epoch $epoch --model $model --out-type $out_type

rm -f $result_dir/vectors.txt.gz
gzip $result_dir/vectors.txt
