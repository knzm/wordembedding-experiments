# -*- coding: utf-8 -*-

from __future__ import print_function

import os
import random

import numpy as np
from gensim.models.word2vec import Word2Vec


def main(result_dir):
    model_path = os.path.join(result_dir, "vectors.bin")
    fvocab = os.path.join(result_dir, "vocab.txt")
    if not os.path.exists(fvocab):
        fvocab = None
    model = Word2Vec.load_word2vec_format(
        model_path, fvocab=fvocab, binary=True)

    valid_size = 16     # Random set of words to evaluate similarity on.
    valid_window = 100  # Only pick dev samples in the head of the distribution.
    valid_examples = np.array(random.sample(np.arange(valid_window), valid_size))

    for i in xrange(valid_size):
        valid_word = model.index2word[valid_examples[i]]
        top_k = 8 # number of nearest neighbors
        nearest = model.most_similar(positive=[valid_word], topn=top_k)
        print(u"Nearest to {0}: {1}".format(
                valid_word,
                ", ".join([word for word, dist in nearest])).encode('utf-8'))


if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
