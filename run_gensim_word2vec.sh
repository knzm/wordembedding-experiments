#!/bin/sh -e

PYTHON=virtualenvs/gensim/bin/python
result_root=${result_root:-result}
result_dir=$result_root/text8_gensim

mkdir -p $result_dir

export result_dir
$PYTHON word2vec_gensim.py

rm -f $result_dir/vectors.txt.gz
gzip $result_dir/vectors.txt
