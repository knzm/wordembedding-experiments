#!/bin/sh -e

tensorflow_ver=0.6.0

virtualenv=${virtualenv:-virtualenv}

$virtualenv virtualenvs/tensorflow-cpu --system-site-packages
virtualenvs/tensorflow-cpu/bin/pip install -U pip

if [ "$(uname)" = "Darwin" ]; then
  # on Mac
  virtualenvs/tensorflow-cpu/bin/pip install https://storage.googleapis.com/tensorflow/mac/tensorflow-${tensorflow_ver}-py2-none-any.whl
else
  # on Linux
  virtualenvs/tensorflow-cpu/bin/pip install https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-${tensorflow_ver}-cp27-none-linux_x86_64.whl
fi
