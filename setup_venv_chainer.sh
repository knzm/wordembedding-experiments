#!/bin/sh -e

chainer_ver=1.5.0.2

virtualenv=${virtualenv:-virtualenv}

$virtualenv virtualenvs/chainer
virtualenvs/chainer/bin/pip install -U pip

for pkg in cython numpy h5py; do
  virtualenvs/chainer/bin/pip install $pkg
done

virtualenvs/chainer/bin/pip install chainer==$chainer_ver
