#!/bin/sh -e

virtualenv=${virtualenv:-virtualenv}

tensorflow_ver=0.6.0

if [ "${BUILD_TENSORFLOW:-0}" -eq 0 ]; then
  if [ "$(uname)" != "Darwin" ]; then
    # on Linux
    $virtualenv virtualenvs/tensorflow-gpu --system-site-packages
    virtualenvs/tensorflow-gpu/bin/pip install -U pip
    virtualenvs/tensorflow-gpu/bin/pip install https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-${tensorflow_ver}-cp27-none-linux_x86_64.whl
  fi
  # Note: for Mac, only CPU-version is available at the moment.

else
  $virtualenv virtualenvs/tensorflow-gpu --system-site-packages
  virtualenvs/tensorflow-gpu/bin/pip install -U pip
  for pkg in numpy; do
    virtualenvs/tensorflow-gpu/bin/pip install $pkg
  done
  (
    . virtualenvs/tensorflow-gpu/bin/activate;
    cd build; 
    ./install_tensorflow.sh
  )
fi
