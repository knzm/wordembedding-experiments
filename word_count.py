# -*- coding: utf-8 -*-

from __future__ import print_function

import collections


def main(args):
    words = []
    words = args.train_file.read().split()
    for word, n in collections.Counter(words).most_common():
        if n >= args.min_count:
            print(word.encode('utf-8'), n)


def get_parser():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("train_file", type=argparse.FileType('r'))
    parser.add_argument("--min-count", type=int, default=0)
    return parser


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    main(args)
