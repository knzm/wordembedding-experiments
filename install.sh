#!/bin/sh -e

mkdir -p bin data text8

cp build/word2vec/word2vec bin/word2vec

if [ -f build/word2vec_cbow/word2vec ]; then
  cp build/word2vec_cbow/word2vec bin/word2vec_cbow
fi

cp build/dmtk/distributed_word_embedding/bin/word_embedding bin/word_embedding
cp build/dmtk/distributed_word_embedding/preprocess/word_count bin/word_count
cp build/dmtk/distributed_word_embedding/preprocess/stopwords_simple.txt text8/stopwords_simple.txt

cp build/dmtk/distributed_skipgram_mixture/bin/distributed_skipgram_mixture bin/distributed_skipgram_mixture

cp build/word2vec/compute-accuracy bin/compute-accuracy
cp build/word2vec/questions-words.txt data/questions-words.txt
