#!/bin/sh -e

PYTHON=virtualenvs/tensorflow-cpu/bin/python
# PYTHON=virtualenvs/tensorflow-gpu/bin/python
result_root=${result_root:-result}
result_dir=$result_root/text8_tensorflow

mkdir -p $result_dir

export result_dir
$PYTHON word2vec_tensorflow.py

rm -f $result_dir/vectors.txt.gz
gzip $result_dir/vectors.txt
