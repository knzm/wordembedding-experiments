#!/bin/sh -e

bindir=./bin
result_root=${result_root:-result}
result_dir=$result_root/text8_dmtk_word_embedding

size=200
read_vocab=text8/vocab.txt
train_file=text8/text8
binary=0
cbow=1
alpha=0.01
epoch=${epoch:-15}
window=8
sample=0
hs=0
negative=25
threads=16
mincount=5
sw_file=text8/stopwords_simple.txt
stopwords=0
data_block_size=1000000000
max_preload_data_size=20000000000
use_adagrad=0

output=${result_dir}/vectors.txt
save_vocab=${result_dir}/vocab.txt

mkdir -p $result_dir
$bindir/word_embedding -max_preload_data_size $max_preload_data_size -alpha $alpha -data_block_size $data_block_size -train_file $train_file -output $output -threads $threads -size $size -binary $binary -cbow $cbow -epoch $epoch -negative $negative -hs $hs -sample $sample -min_count $mincount -window $window -stopwords $stopwords -sw_file $sw_file -read_vocab $read_vocab -use_adagrad $use_adagrad 
cp $read_vocab $save_vocab

rm -f $result_dir/vectors.txt.gz
gzip $result_dir/vectors.txt
