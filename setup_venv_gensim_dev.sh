#!/bin/sh -e

virtualenv=${virtualenv:-virtualenv}

branch=${1:-master}

$virtualenv virtualenvs/gensim-${branch}
virtualenvs/gensim-${branch}/bin/pip install -U pip

for pkg in cython numpy scipy; do
  virtualenvs/gensim-${branch}/bin/pip install $pkg
done

(
  . virtualenvs/gensim-${branch}/bin/activate;
  cd build;
  ./install_gensim.sh ${branch}
)
