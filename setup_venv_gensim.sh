#!/bin/sh -e

gensim_ver=0.12.3

virtualenv=${virtualenv:-virtualenv}

$virtualenv virtualenvs/gensim
virtualenvs/gensim/bin/pip install -U pip

for pkg in cython numpy scipy; do
  virtualenvs/gensim/bin/pip install $pkg
done

virtualenvs/gensim/bin/pip install gensim==$gensim_ver
